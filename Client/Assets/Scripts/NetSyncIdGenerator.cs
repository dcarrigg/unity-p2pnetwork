﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

// Generates network sync ids for scene objects
public class NetSyncIdGenerator : AssetModificationProcessor
{
    public static string[] OnWillSaveAssets(string[] paths)
    {
        // Get the name of the scene to save.
        string scenePath = string.Empty;
        string sceneName = string.Empty;

        foreach (string path in paths)
        {
            if (path.Contains(".unity"))
            {
                scenePath = Path.GetDirectoryName(path);
                sceneName = Path.GetFileNameWithoutExtension(path);

                // Check to make sure all UniqueId scripts have a unique id
                int lastUniqueId = 1;
                NetworkSync[] objects = Resources.FindObjectsOfTypeAll<NetworkSync>();
                foreach (NetworkSync uid in objects)
                {
                    if (UnityEditor.EditorUtility.IsPersistent(uid.transform.root.gameObject))
                    {
                        continue;
                    }
                    uid.sceneId = uid.gameObject.scene.name + lastUniqueId++;
                    UnityEditor.EditorUtility.SetDirty(uid);
                    //Debug.Log("Id " + uid.sceneId + "Test");
                }
            }
        }
        return paths;
    }
}
