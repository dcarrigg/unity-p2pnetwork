﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Steamworks;

public class GameClient : MonoBehaviour
{
    public P2PClientNetwork clientNet;
    
    // Use this for initialization
    void Awake()
    {
        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<P2PClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (P2PClientNetwork)gameObject.AddComponent(typeof(P2PClientNetwork));
        }
    }

    void OnDestroy()
    {
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect();
        }
    }

    // We've just entered a steam lobby
    public void SteamLobbyEntered()
    {
        // Load the map
        SceneManager.LoadScene("DemoScene");
    }

    // Called by the other clients when they have finished sending us data about the scene we are in
    public void FinishedInitializationData(ulong aClientId)
    {

    }
}
