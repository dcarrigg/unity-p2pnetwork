﻿using UnityEngine;
using System.Collections;

// What "channel" are we currently talking on
public enum VoiceChatChannel {
    Proximity = 1,
    Radio = 2,
    Global = 3
}