﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using System.IO;
using System.Reflection;

// Peer to Peer connection library using the Steamworks SDK
public class P2PClientNetwork : MonoBehaviour
{

    protected Callback<P2PSessionRequest_t> peerToPeerSessionStart;
    protected Callback<LobbyCreated_t> gameLobbyCreated;
    protected Callback<GameLobbyJoinRequested_t> lobbyInviteReceived;
    protected Callback<LobbyDataUpdate_t> lobbyUpdated;
    protected Callback<LobbyEnter_t> lobbyEntered;
    protected Callback<LobbyChatUpdate_t> lobbyChatUpdate;

    // Information about our friends
    public class FriendInfo
    {
        public string friendName;
        public CSteamID steamId;
        public EPersonaState state;
        public FriendGameInfo_t gameInfo;
        public bool inGame;
    }

    // The steam lobby we are currently in
    public CSteamID lobbyId;
    CSteamID friendToInvite;
    List<CSteamID> lobbyMembers = new List<CSteamID>();

    // Steam Id of the sender of the last received RPC
    public CSteamID sendingClientId;

    // List of networkIds that this client can use to create networked objects
    // TODO: How do we allocate new ids? Start at 1M * client # in the lobby?
    Queue<int> freeIds = new Queue<int>();

    // List of ids this owns
    List<int> ownedIds = new List<int>();

    // This is the current area this client is in
    List<int> areaIds = new List<int>();

    // Maps networkIds to their gameobject and networkId component
    protected class NetIdMapData
    {
        public GameObject gameObject;
        public NetworkSync networkSync;
        public VoiceChatPlayer voiceChatPlayer;
    };
    protected Dictionary<int, NetIdMapData> netIdObjMap = new Dictionary<int, NetIdMapData>();

    // AudioSources for voice chat from objects not in our area
    Dictionary<int, VoiceChatPlayer> remoteVoiceChatPlayers = new Dictionary<int, VoiceChatPlayer>();
    // This object contains all of the audio souces for playing voice chat from non-local objects
    // As new objects chat remotely, audio sources for them will be added as child objects to this object
    public GameObject remoteVoiceChatContainerObject;
    // This is the prefab object we'll instantiate for playing remote audio. Make sure this has an audio source component and a VoiceChatPlayer component
    public GameObject remoteVoiceChatPrefab;

#if UNITY_EDITOR
    float lastRPCCallClearTime;
    int RPCCallWarningLevel = 20;
    Dictionary<string, int> recentRPCCalls = new Dictionary<string, int>();
#endif

    // Delivery methods
    public enum DeliveryMethod
    {
        ReliableOrdered = EP2PSend.k_EP2PSendReliable,
        UnreliableSequenced = EP2PSend.k_EP2PSendUnreliable
    };

    // Receiver type
    public enum MessageReceiver
    {
        ServerOnly = 1,
        AllClients = 2,
        OtherClients = 4,
        AllClientsInArea = 8,
        OtherClientsInArea = 16,
        SingleClient = 32
    };

    // The types of messages we'll be sending between the server/clients
    protected enum MessageType
    {
        SyncUpdate = 1,
        Instantiate = 2,
        AddToArea = 3,
        RemoveFromArea = 4,
        RPC = 5,
        IdAllocation = 6,
        OwnershipGained = 7,
        OwnershipLost = 8,
        Destroy = 9,
        VoiceData = 10,
        LiteSyncUpdate = 11,
        ConnectNetworkSync = 12,
        AddObjectToArea = 13,
        RemoveObjectFromArea = 14
    };

    private void Awake()
    {
        // TODO: How do we handle id allocation?
        for (int i = 0; i < 10000; i++)
        {
            freeIds.Enqueue(UnityEngine.Random.Range(1, 99999999));
        }

#if UNITY_EDITOR
        lastRPCCallClearTime = Time.realtimeSinceStartup;
#endif
    }

    private void OnEnable()
    {
        // Set up the steam callbacks
        if (SteamManager.Initialized)
        {
            gameLobbyCreated = Callback<LobbyCreated_t>.Create(OnGameLobbyCreated);
            lobbyInviteReceived = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyInviteReceived);
            lobbyUpdated = Callback<LobbyDataUpdate_t>.Create(OnGameLobbyUpdate);
            lobbyEntered = Callback<LobbyEnter_t>.Create(OnGameLobbyEnter);
            peerToPeerSessionStart = Callback<P2PSessionRequest_t>.Create(OnPeerToPeerSessionStart);
            lobbyChatUpdate = Callback<LobbyChatUpdate_t>.Create(OnLobbyChatUpdate);
        }
        else
        {
            Debug.LogError("SteamManager not initialized. Cannot create callback methods.");
        }
    }

    void OnDisable()
    {
        Disconnect();
    }

    // Called when another client sends data to this client via the steam network
    void OnPeerToPeerSessionStart(P2PSessionRequest_t aSessionData)
    {
        Debug.Log("Accepting a p2p session with " + aSessionData.m_steamIDRemote.m_SteamID);

        // TODO: Should we accept this connection? Probably only if they are in the lobby with us.
        SteamNetworking.AcceptP2PSessionWithUser(aSessionData.m_steamIDRemote);
    }

    void OnLobbyChatUpdate(LobbyChatUpdate_t aChatUpdate)
    {
        uint leftBits = (uint)EChatMemberStateChange.k_EChatMemberStateChangeDisconnected
            | (uint)EChatMemberStateChange.k_EChatMemberStateChangeLeft
            | (uint)EChatMemberStateChange.k_EChatMemberStateChangeKicked;

        if ((aChatUpdate.m_rgfChatMemberStateChange & leftBits) != 0)
        {
            PlayerDisconnected(new CSteamID(aChatUpdate.m_ulSteamIDUserChanged));
            //SendMessage("PlayerDisconnected", new CSteamID(aChatUpdate.m_ulSteamIDUserChanged), SendMessageOptions.DontRequireReceiver);
        }
        if ((aChatUpdate.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeEntered) != 0)
        {
            PlayerConnected(new CSteamID(aChatUpdate.m_ulSteamIDUserChanged));
            //SendMessage("PlayerConnected", new CSteamID(aChatUpdate.m_ulSteamIDUserChanged), SendMessageOptions.DontRequireReceiver);
        }
    }

    void OnGameLobbyCreated(LobbyCreated_t aLobbyResult)
    {
        Debug.Log("Lobby created " + aLobbyResult.m_ulSteamIDLobby);
        if (aLobbyResult.m_eResult == EResult.k_EResultOK)
        {
            // Invite the other user
            lobbyId = new CSteamID(aLobbyResult.m_ulSteamIDLobby);
            if (friendToInvite != null)
            {
                SteamMatchmaking.InviteUserToLobby(lobbyId, friendToInvite);
            }
        }
        else
        {
            Debug.LogError("Unable to create steam lobby " + aLobbyResult.m_eResult);
        }
    }

    void OnGameLobbyInviteReceived(GameLobbyJoinRequested_t aInviteData)
    {
        Debug.Log("You've been invited to a lobby");
        SteamMatchmaking.JoinLobby(aInviteData.m_steamIDLobby);
    }

    void OnGameLobbyUpdate(LobbyDataUpdate_t aLobbyData)
    {
        Debug.Log("Lobby update " + aLobbyData.m_ulSteamIDLobby);
    }

    void OnGameLobbyEnter(LobbyEnter_t aLobbyData)
    {
        Debug.Log("Lobby entered " + aLobbyData.m_ulSteamIDLobby);
        lobbyId = new CSteamID(aLobbyData.m_ulSteamIDLobby);

        SendMessage("SteamLobbyEntered");
    }

    void PlayerConnected(CSteamID aSteamId)
    {
        Debug.Log("Player has connected");

        // Send initialization data for any object we own
        foreach (int id in ownedIds)
        {
            MemoryStream stream = new MemoryStream();
            BinaryWriter sendMsg = new BinaryWriter(stream);
            if (netIdObjMap[id].networkSync.sceneId != "")
            {
                // Let the client know this scene object should be connected to a net id
                sendMsg.Write((int)MessageType.ConnectNetworkSync);
                sendMsg.Write(netIdObjMap[id].networkSync.position.x);
                sendMsg.Write(netIdObjMap[id].networkSync.position.y);
                sendMsg.Write(netIdObjMap[id].networkSync.position.z);
                sendMsg.Write(netIdObjMap[id].networkSync.rotation.x);
                sendMsg.Write(netIdObjMap[id].networkSync.rotation.y);
                sendMsg.Write(netIdObjMap[id].networkSync.rotation.z);
                sendMsg.Write(netIdObjMap[id].networkSync.rotation.w);
                sendMsg.Write(netIdObjMap[id].networkSync.gameObject.name);
                sendMsg.Write(netIdObjMap[id].networkSync.sceneId);
                sendMsg.Write(netIdObjMap[id].networkSync.GetId());
            }
            else
            {
                // Let the client know about the new object
                sendMsg.Write((int)MessageType.Instantiate);
                sendMsg.Write(id);
                sendMsg.Write(netIdObjMap[id].networkSync.prefabName);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localPosition.x);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localPosition.y);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localPosition.z);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localRotation.x);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localRotation.y);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localRotation.z);
                sendMsg.Write(netIdObjMap[id].gameObject.transform.localRotation.w);
                sendMsg.Write(""); // TODO: Is there any json data we want to send over the network?
            }

            // Send the message
            SendData(MessageReceiver.SingleClient, ref stream, DeliveryMethod.ReliableOrdered, 0, aSteamId);

            // Send a message to the scripts on the player object to send initilization infor
            netIdObjMap[id].gameObject.SendMessage("SendInitializationData", aSteamId, SendMessageOptions.DontRequireReceiver);

            // Send a sync message about the object
            netIdObjMap[id].networkSync.SendSyncData(aSteamId);
        }

        // Tell the client they've received all the sync data from us
        CallRPC("FinishedInitializationData", aSteamId, -1, SteamUser.GetSteamID().m_SteamID);
        // SendData(MessageReceiver.SingleClient, ref stream, DeliveryMethod.ReliableOrdered, 0, aSteamId);
    }

    // Am I the owner of the steam lobby
    bool IsLobbyOwner()
    {
        return SteamMatchmaking.GetLobbyOwner(lobbyId) == SteamUser.GetSteamID();
    }

    // Get the owner of the lobby
    public CSteamID GetLobbyOwner()
    {
        return SteamMatchmaking.GetLobbyOwner(lobbyId);
    }

    void PlayerDisconnected(CSteamID aSteamId)
    {
        Debug.Log("Player has disconnected");

        // Always offload everything to the lobby owner, then they can delegate
        if (IsLobbyOwner())
        {
            // I take ownership of all of this client's objects
            Debug.Log("I am the owner!");

            // TODO: Gain ownership of all of the game objects

        }
    }

    // Get a list of this user's steam friends
    public List<FriendInfo> GetFriends()
    {
        List<FriendInfo> friends = new List<FriendInfo>();
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            EPersonaState friendState = SteamFriends.GetFriendPersonaState(friendSteamId);
            FriendGameInfo_t friendGameInfo;
            bool friendInGame = SteamFriends.GetFriendGamePlayed(friendSteamId, out friendGameInfo);

            FriendInfo info = new FriendInfo();
            info.friendName = friendName;
            info.steamId = friendSteamId;
            info.state = friendState;
            info.gameInfo = friendGameInfo;
            info.inGame = friendInGame;

            friends.Add(info);
        }

        return friends;
    }

    // Join into a lobby with another player
    public void JoinFriend(CSteamID aSteamId)
    {
        // If the friend is already in a game, see if we can join them
        FriendGameInfo_t gameInfo;
        SteamFriends.GetFriendGamePlayed(aSteamId, out gameInfo);
        if (gameInfo.m_steamIDLobby.IsValid())
        {
            // Try to join the friend's lobby
            SteamMatchmaking.JoinLobby(gameInfo.m_steamIDLobby);
        }
        else
        {
            // If the friend is online but not playing our game, start by creating a new lobby to invite them to
            friendToInvite = aSteamId;
            SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
        }
    }

    // Create a new game lobby
    public void CreateGame()
    {
        SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
    }

    // Send data over the network, using a MemoryStream
    void SendData(MessageReceiver aReceiver, ref MemoryStream aData, DeliveryMethod aDeliveryMethod, int aChannel = 0, CSteamID aTarget = new CSteamID())
    {
        byte[] data = aData.ToArray();
        SendData(aReceiver, ref data, aDeliveryMethod, aChannel, aTarget);
    }
    // Send data over the network
    void SendData(MessageReceiver aReceiver, ref byte[] aData, DeliveryMethod aDeliveryMethod, int aChannel = 0, CSteamID aTarget = new CSteamID())
    {
        // Use the steam networking api
        // bool ReadP2PPacket( void *pubDest, uint32 cubDest, uint32 *pcubMsgSize, CSteamID *psteamIDRemote, int nChannel = 0 );

        // Send to everyone currently in the lobby
        if (aReceiver == MessageReceiver.AllClients || aReceiver == MessageReceiver.AllClientsInArea)
        {
            for (int i = 0; i < SteamMatchmaking.GetNumLobbyMembers(lobbyId); i++)
            {
                CSteamID otherId = SteamMatchmaking.GetLobbyMemberByIndex(lobbyId, i);
                SteamNetworking.SendP2PPacket(otherId, aData, (uint)aData.Length, (EP2PSend)aDeliveryMethod);
            }
        }
        else if (aReceiver == MessageReceiver.OtherClients || aReceiver == MessageReceiver.OtherClientsInArea)
        {
            for (int i = 0; i < SteamMatchmaking.GetNumLobbyMembers(lobbyId); i++)
            {
                CSteamID otherId = SteamMatchmaking.GetLobbyMemberByIndex(lobbyId, i);
                if (SteamUser.GetSteamID() != otherId)
                {
                    SteamNetworking.SendP2PPacket(otherId, aData, (uint)aData.Length, (EP2PSend)aDeliveryMethod);
                }
            }
        }
        else if (aReceiver == MessageReceiver.SingleClient)
        {
            SteamNetworking.SendP2PPacket(aTarget, aData, (uint)aData.Length, (EP2PSend)aDeliveryMethod);
        }
        else if (aReceiver == MessageReceiver.ServerOnly)
        {
            CSteamID otherId = SteamMatchmaking.GetLobbyOwner(lobbyId);
            SteamNetworking.SendP2PPacket(otherId, aData, (uint)aData.Length, (EP2PSend)aDeliveryMethod);
        }
        else
        {
            Debug.Log("Unsupported MessageReceiver " + aReceiver);
        }
    }

    // Disconnect from the network
    public void Disconnect()
    {
        DestroyNetworkObjects(true);

        if (SteamManager.Initialized)
            SteamMatchmaking.LeaveLobby(lobbyId);
    }

    // Function called when we get a data message
    protected void HandleIncomingData(ref BinaryReader aMsg)
    {
        // Data has just been sent from the server, what to do with it...
        MessageType type = (MessageType)aMsg.ReadInt32();
        //Debug.Log("Received message with type " + type);
        if (type == MessageType.RPC)
        {
            ReceiveRPC(aMsg);
        }
        else if (type == MessageType.AddToArea)
        {
            int newAreaId = aMsg.ReadInt32();

            // Add this area id to the list of areas we are in
            areaIds.Add(newAreaId);
            SendMessage("OnChangeArea", SendMessageOptions.DontRequireReceiver);

        }
        else if (type == MessageType.RemoveFromArea)
        {
            int oldAreaId = aMsg.ReadInt32();

            // Remove the area id from the list of areas we are in
            areaIds.Remove(oldAreaId);
        }
        else if (type == MessageType.AddObjectToArea || type == MessageType.RemoveObjectFromArea)
        {
            // TODO: Unimplemented
        }
        else if (type == MessageType.IdAllocation)
        {
            // Add the new id to our list of free ids
            int startId = aMsg.ReadInt32();
            int endId = aMsg.ReadInt32();
            //Debug.Log("Client adding ids " + startId.ToString() + " to " + endId.ToString() + " to its free ids");
            for (; startId < endId; startId++)
            {
                freeIds.Enqueue(startId);
            }

        }
        else if (type == MessageType.SyncUpdate || type == MessageType.LiteSyncUpdate)
        {
            aMsg.ReadInt32(); // Read the receiver value, which we don't care about here
            int networkId = aMsg.ReadInt32();
            Vector3 position = new Vector3();
            Quaternion orientation = new Quaternion();
            if (type == MessageType.SyncUpdate)
            {
                float x = aMsg.ReadSingle();
                float y = aMsg.ReadSingle();
                float z = aMsg.ReadSingle();
                position = new Vector3(x, y, z);

                x = aMsg.ReadSingle();
                y = aMsg.ReadSingle();
                z = aMsg.ReadSingle();
                float w = aMsg.ReadSingle();
                orientation = new Quaternion(x, y, z, w);
            }

            int byteDataLength = aMsg.ReadInt32();
            byte[] byteData = aMsg.ReadBytes(byteDataLength); // TODO: We could do some better checking here

            // Tell the NetworkSync component about the incoming data
            if (!netIdObjMap.ContainsKey(networkId))
            {
                Debug.LogWarning("Received a SyncUpdate for a networkId this client doesn't know about: " + networkId);
                return;
            }
            if (type == MessageType.SyncUpdate)
            {
                netIdObjMap[networkId].networkSync.ReceiveSyncData(byteData, position, orientation);
            }
            else if (type == MessageType.LiteSyncUpdate)
            {
                netIdObjMap[networkId].networkSync.ReceiveLiteSyncData(byteData);
            }
        }
        else if (type == MessageType.Instantiate)
        {
            int netId = aMsg.ReadInt32();
            string prefabName = aMsg.ReadString();

            float x = aMsg.ReadSingle();
            float y = aMsg.ReadSingle();
            float z = aMsg.ReadSingle();
            Vector3 position = new Vector3(x, y, z);

            x = aMsg.ReadSingle();
            y = aMsg.ReadSingle();
            z = aMsg.ReadSingle();
            float w = aMsg.ReadSingle();
            Quaternion orientation = new Quaternion(x, y, z, w);

            string json = aMsg.ReadString();

            Debug.Log("Instantiating " + prefabName + " with id " + netId);

            // Create the object locally
            GameObject tempObj = CreateLocalObject(prefabName, position, orientation, netId);

            if (tempObj != null && !string.IsNullOrEmpty(json))
            {
                tempObj.SendMessage("ParseJson", json);
            }
        }
        else if (type == MessageType.ConnectNetworkSync)
        {
            // Someone is trying to find out what network id a scene object should have
            // Find out what the id is and send it back to them
            // Position
            float xPos = aMsg.ReadSingle();
            float yPos = aMsg.ReadSingle();
            float zPos = aMsg.ReadSingle();

            // Rotation
            float xRot = aMsg.ReadSingle();
            float yRot = aMsg.ReadSingle();
            float zRot = aMsg.ReadSingle();
            float wRot = aMsg.ReadSingle();

            string name = aMsg.ReadString();
            string sceneId = aMsg.ReadString();
            int networkId = aMsg.ReadInt32();

            Vector3 position = new Vector3(xPos, yPos, zPos);

            // Find the net sync based on sceneId
            NetworkSync[] netSyncs = FindObjectsOfType<NetworkSync>();
            if (sceneId != String.Empty)
            {
                for (int i = 0; i < netSyncs.Length; i++)
                {
                    if (netSyncs[i].sceneId == sceneId)
                    {
                        //Debug.Log("Assigning a network id for network sync " + netSyncs[i].sceneId);
                        netSyncs[i].clientNet = this;
                        netSyncs[i].SetId(networkId);
                        return;
                    }
                }
            }

            /*
            // Old failsafe: If this hasn't found the object yet, find it by position.
            for (int i = 0; i < netSyncs.Length; i++)
            {
                if (netSyncs[i].position == position)
                {
                    Debug.LogWarning("Using position to assigning a network id for network sync " + netSyncs[i].name);
                    netSyncs[i].clientNet = this;
                    netSyncs[i].SetId(networkId);
                    return;
                }
            }
            */
            Debug.LogError("ConnectNetworkSync received, but was unable to find an unconnected network sync.");
        }
        else if (type == MessageType.OwnershipGained)
        {
            // Gain ownership of a specific object
            int objectId = aMsg.ReadInt32();

            AddOwnedId(objectId);
        }
        else if (type == MessageType.OwnershipLost)
        {
            // Gain ownership of a specific object
            int objectId = aMsg.ReadInt32();

            RemoveOwnedId(objectId);
        }
        else if (type == MessageType.Destroy)
        {
            // Destroy the object with the given network id
            int objectId = aMsg.ReadInt32();

            DestroyNetworkObjects(objectId);
        }
        else if (type == MessageType.VoiceData)
        {
            //Speed back playback!

            int networkObjectId = aMsg.ReadInt32();

            VoiceChatPacket packet = new VoiceChatPacket();
            packet.Compression = (VoiceChatCompression)aMsg.ReadInt32();
            packet.NetworkId = aMsg.ReadInt32();
            VoiceChatChannel channel = (VoiceChatChannel)aMsg.ReadInt32();
            packet.Length = aMsg.ReadInt32();
            packet.Data = aMsg.ReadBytes(packet.Length);

            // Find out which object should be playing this audio, send it to them
            switch (channel)
            {
                case VoiceChatChannel.Radio:
                    // If this is an object that isn't in our area, or isn't a specific object at all, set up a new audio source to play from
                    if (!remoteVoiceChatPlayers.ContainsKey(networkObjectId))
                    {

                        GameObject newVoiceChatObj = Instantiate(remoteVoiceChatPrefab);
                        newVoiceChatObj.transform.SetParent(remoteVoiceChatContainerObject.transform, false);
                        newVoiceChatObj.name = "RemoteVoiceChat - " + networkObjectId.ToString();

                        VoiceChatPlayer vcp = newVoiceChatObj.GetComponent<VoiceChatPlayer>();
                        vcp.sourceNetId = networkObjectId;
                        vcp.isRadio = true;

                        remoteVoiceChatPlayers[networkObjectId] = vcp;

                    }
                    remoteVoiceChatPlayers[networkObjectId].OnNewSample(packet);
                    break;
                case VoiceChatChannel.Proximity:
                    if (netIdObjMap.ContainsKey(networkObjectId))
                    {
                        if (netIdObjMap[networkObjectId].voiceChatPlayer == null)
                        {
                            // If they dont have a voice chat player, set one up real quick (requires an AudioSource)
                            if (netIdObjMap[networkObjectId].gameObject.GetComponent<VoiceChatPlayer>() == null)
                            {
                                netIdObjMap[networkObjectId].voiceChatPlayer = netIdObjMap[networkObjectId].gameObject.AddComponent<VoiceChatPlayer>();
                            }
                        }
                        else
                        {
                            if (netIdObjMap[networkObjectId].gameObject.GetComponent<VoiceChatPlayer>() == null)
                            {
                                netIdObjMap[networkObjectId].voiceChatPlayer = netIdObjMap[networkObjectId].gameObject.AddComponent<VoiceChatPlayer>();
                            }
                        }

                        netIdObjMap[networkObjectId].voiceChatPlayer.OnNewSample(packet);
                    }
                    else
                    {
                        Debug.Log("Proximity VoiceData has been received by this client, but it doesn't have a network object with id " + networkObjectId);
                    }
                    break;
            }
        }
        else
        {
            Debug.LogError("Unhandled message type sent to client " + type.ToString());
        }
    }


    // Call a RPC on a single client
    public void CallRPC(string aFunctionName, CSteamID aClientId, int aNetworkId, params object[] aParams)
    {
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.RPC); // This is an rpc
        sendMsg.Write((int)MessageReceiver.SingleClient); // Which other clients this should be sent to
        sendMsg.Write(aNetworkId); // What network object on the clients this should be sent to
        sendMsg.Write(aFunctionName); // What network object on the clients this should be sent to

        // Write the RPC parameters
        WriteRPCParams(ref sendMsg, aParams);

        // Send the message
        SendData(MessageReceiver.SingleClient, ref stream, DeliveryMethod.ReliableOrdered, 0, aClientId);
    }

    // Call a RPC to a group of clients
    public void CallRPC(string aFunctionName, MessageReceiver aReceiver, int aNetworkId, params object[] aParams)
    {
        CallRPC(aFunctionName, aReceiver, aNetworkId, DeliveryMethod.ReliableOrdered, aParams);
    }

    // Call a RPC to a group of clients with a specific delivery method
    public void CallRPC(string aFunctionName, MessageReceiver aReceiver, int aNetworkId, DeliveryMethod aDeliveryMethod, params object[] aParams)
    {
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.RPC); // This is an rpc
        sendMsg.Write((int)aReceiver); // Which other clients this should be sent to
        sendMsg.Write(aNetworkId); // What network object on the clients this should be sent to
        sendMsg.Write(aFunctionName); // What network object on the clients this should be sent to

        // Write the RPC parameters
        WriteRPCParams(ref sendMsg, aParams);

        // Send the message
        SendData(aReceiver, ref stream, aDeliveryMethod);

#if UNITY_EDITOR
        // If it's been longer than 1 second since we last cleared, clear now
        if (Time.realtimeSinceStartup - lastRPCCallClearTime > 1.0)
        {
            recentRPCCalls.Clear();
            lastRPCCallClearTime = Time.realtimeSinceStartup;
        }
        if (recentRPCCalls.ContainsKey(aFunctionName))
        {
            recentRPCCalls[aFunctionName] += 1;
        }
        else
        {
            recentRPCCalls[aFunctionName] = 1;
        }
        int totalRPCCalls = 0;
        string logString = "";
        foreach (KeyValuePair<string, int> element in recentRPCCalls)
        {
            totalRPCCalls += element.Value;
            logString += element.Key + ": " + element.Value + "\n";
        }
        if (totalRPCCalls > RPCCallWarningLevel)
        {
            Debug.LogWarning(totalRPCCalls + " RPC calls made in the last second. Types are: \n" + logString);
        }
#endif
    }


    protected void ReceiveRPC(BinaryReader aMsg)
    {
        aMsg.ReadInt32(); // Read the receiver value, which we don't care about here
        int networkId = aMsg.ReadInt32();
        string functionName = aMsg.ReadString();
        //print("Got RPC: " + functionName);
        // Get the arguments
        List<object> args = new List<object>();
        ReadRPCParams(ref args, ref aMsg);

        // Find the object with this networkId
        GameObject targetObject;
        if (networkId == -1)
        {
            targetObject = this.gameObject;
        }
        else
        {
            if (!netIdObjMap.ContainsKey(networkId))
            {
                Debug.LogError(functionName + " was sent to unknown net object " + networkId.ToString());
                return;
            }
            targetObject = netIdObjMap[networkId].gameObject;
            if (targetObject == null)
            {
                Debug.LogError("Unable to get object from id map " + networkId.ToString());
                return;
            }
        }

        // Find the method to call
        // Loop through all the script components, finding a function
        bool rpcSent = false;
        MethodInfo theMethod = null;
        Component targetScript = null;
        Component[] scripts = targetObject.GetComponents(typeof(MonoBehaviour));
        for (int i = 0; i < scripts.Length; i++)
        {
            theMethod = null;
            // Debug.Log(scripts[i].GetType().ToString());
            theMethod = scripts[i].GetType().GetMethod(functionName);
            if (theMethod != null)
            {
                if (!rpcSent)
                    rpcSent = true;

                targetScript = scripts[i];

                try
                {
                    theMethod.Invoke(targetScript, args.ToArray());
                }
                catch (TargetParameterCountException exp)
                {
                    Debug.LogError(targetScript + "'s function " + theMethod + " has a parameter count exception.");
                    Debug.LogError(exp.ToString());
                }
                catch (TargetException e)
                {
                    Debug.Log(functionName);
                    Debug.LogError("Unable to call RPC method: " + e.ToString());
                }
                catch (Exception exp)
                {
                    Debug.LogError(targetScript + "'s function " + theMethod + " has an exception.");
                    Debug.LogError(exp.ToString());
                }
                //break;
            }
        }
        if (!rpcSent)
        {
            GameObject obj = GetGameObject(networkId);
            string objectInfo = objectInfo = " null object with net id " + networkId;
            if (obj != null)
            {
                objectInfo = obj.name;
            }
            Debug.LogWarning("Unhandled RPC call to function " + functionName + " for object " + objectInfo);
            return;
        }

        // Call the method

    }

    // Send data about a sync'ed networked object to a single client
    public void SyncNetworkDataSingleClient(int aNetworkId, CSteamID aClientId, DeliveryMethod aDeliveryMethod, byte[] aData, Vector3 aPosition, Quaternion aRotation)
    {
        if (!IsConnected())
        {
            return;
        }
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.SyncUpdate);
        sendMsg.Write((int)MessageReceiver.SingleClient);
        sendMsg.Write(aNetworkId);
        sendMsg.Write(aPosition.x);
        sendMsg.Write(aPosition.y);
        sendMsg.Write(aPosition.z);
        sendMsg.Write(aRotation.x);
        sendMsg.Write(aRotation.y);
        sendMsg.Write(aRotation.z);
        sendMsg.Write(aRotation.w);
        sendMsg.Write(aData.Length);
        sendMsg.Write(aData);

        // Send the message to the server
        SendData(MessageReceiver.SingleClient, ref stream, aDeliveryMethod, 0, aClientId);
    }
    
    // Send data about a sync'ed networked object to the server
    public void SyncNetworkData(int aNetworkId, MessageReceiver aMessageReceiver, DeliveryMethod aDeliveryMethod, byte[] aData, Vector3 aPosition, Quaternion aRotation)
    {
        if (!IsConnected())
        {
            return;
        }
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.SyncUpdate);
        sendMsg.Write((int)aMessageReceiver);
        sendMsg.Write(aNetworkId);
        sendMsg.Write(aPosition.x);
        sendMsg.Write(aPosition.y);
        sendMsg.Write(aPosition.z);
        sendMsg.Write(aRotation.x);
        sendMsg.Write(aRotation.y);
        sendMsg.Write(aRotation.z);
        sendMsg.Write(aRotation.w);
        sendMsg.Write(aData.Length);
        sendMsg.Write(aData);

        // Send the message to the server
        SendData(aMessageReceiver, ref stream, aDeliveryMethod);
    }

    // Send data about a sync'ed network object to server that has not changed it's position or rotation
    public void LiteSyncNetworkData(int aNetworkId, MessageReceiver aMessageReceiver, DeliveryMethod aDeliveryMethod, byte[] aData)
    {
        if (!IsConnected())
        {
            return;
        }
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.LiteSyncUpdate);
        sendMsg.Write((int)aMessageReceiver);
        sendMsg.Write(aNetworkId);
        sendMsg.Write(aData.Length);
        sendMsg.Write(aData);

        // Send the message to the server
        SendData(aMessageReceiver, ref stream, aDeliveryMethod);
    }

    // Get the current area id
    public List<int> GetAreas()
    {
        return areaIds;
    }

    // Change the area this client is in
    public void AddToArea(int aNewAreaId)
    {
        //Debug.Log("AddToArea request: " + aNewAreaId);
        if (areaIds.Contains(aNewAreaId))
        {
            return;
        }

        // Send a message to the server letting it know we want to go to a new area
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.AddToArea);
        sendMsg.Write(aNewAreaId);
        // Send the message
        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.ReliableOrdered);
    }

    public void RemoveFromArea(int aOldAreaId)
    {
        //Debug.Log("RemoveFromArea request: " + aOldAreaId);
        if (!areaIds.Contains(aOldAreaId))
        {
            return;
        }

        // Send a message to the server letting it know we want to go to a new area
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.RemoveFromArea);
        sendMsg.Write(aOldAreaId);
        // Send the message
        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.ReliableOrdered);
    }

    public void AddObjectToArea(int aObjectId, int aNewAreaId)
    {
        //Debug.Log("AddObjectToArea request: " + aNewAreaId);

        // Send a message to the server letting it know we want to go to a new area
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.AddObjectToArea);
        sendMsg.Write(aObjectId);
        sendMsg.Write(aNewAreaId);
        // Send the message
        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.ReliableOrdered);
    }

    public void RemoveObjectFromArea(int aObjectId, int aOldAreaId)
    {
        //Debug.Log("RemoveObjectFromArea request: " + aOldAreaId);

        // Send a message to the server letting it know we want to go to a new area
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.RemoveObjectFromArea);
        sendMsg.Write(aObjectId);
        sendMsg.Write(aOldAreaId);
        // Send the message
        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.ReliableOrdered);
    }

    // Instantiate a new object over the network
    public GameObject Instantiate(string prefabName, Vector3 aPostion, Quaternion aOrientation)
    {
        if (!IsConnected())
        {
            Debug.Log("ClientNetwork::Instantiate called while not connected to a server");
            return null;
        }
        // Get a free id
        if (freeIds.Count <= 0)
        {
            Debug.LogError("ClientNetwork::Instantiate, no more network ids available");
            return null;
        }
        int newId = freeIds.Dequeue();

        Debug.Log("ClientNetwork::Instantiate - Creating new obj with id " + newId + "(" + prefabName + ")");

        // Create the object locally
        GameObject newObj = CreateLocalObject(prefabName, aPostion, aOrientation, newId);

        if (newObj == null)
        {
            freeIds.Enqueue(newId); // Put the id back
            return null;
        }

        // Let the server know about the new object
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.Instantiate);
        sendMsg.Write(newId);
        sendMsg.Write(prefabName);
        sendMsg.Write(aPostion.x);
        sendMsg.Write(aPostion.y);
        sendMsg.Write(aPostion.z);
        sendMsg.Write(aOrientation.x);
        sendMsg.Write(aOrientation.y);
        sendMsg.Write(aOrientation.z);
        sendMsg.Write(aOrientation.w);
        sendMsg.Write(""); // TODO: Is there any json data we want to send over the network?
        SendData(MessageReceiver.OtherClients, ref stream, DeliveryMethod.ReliableOrdered);

        // Make sure we know we own this object
        AddOwnedId(newId);

        // Return the new local object
        return newObj;
    }

    private GameObject CreateLocalObject(string aPrefabName, Vector3 aPosition, Quaternion aOrientation, int aNetId)
    {
        // Create the object locally
        GameObject newObj = Instantiate(Resources.Load(aPrefabName), aPosition, aOrientation) as GameObject;
        if (newObj == null)
        {
            Debug.LogError("ClientNetwork.CreateLocalObject can't instantiate unknown prefab " + aPrefabName);
            return null;
        }

        // Set the id on the NetworkId script
        NetworkSync netSync = newObj.GetComponent(typeof(NetworkSync)) as NetworkSync;
        if (netSync != null)
        {
            netSync.clientNet = this;
            netSync.prefabName = aPrefabName;
            netSync.SetId(aNetId);
        }
        else
        {
            Debug.LogWarning("Instantiating object " + gameObject.name + " over the network without a NetworkSync");
        }

        // Tell the object everything for the network has been initialized
        newObj.SendMessage("NetworkInitialized", SendMessageOptions.DontRequireReceiver);


        return newObj;
    }

    // A network sync wants to be assigned a network id
    public void ConnectNetworkSync(NetworkSync aNetworkSync)
    {
        aNetworkSync.clientNet = this;
        
        // TODO: Network syncs no longer request to be connected, and we should instead revieve the data when we connect to the game
        // If we are the host of the lobby, give these network syncs new ids and gain control of these network objects
        if (IsLobbyOwner())
        {
            aNetworkSync.SetId(freeIds.Dequeue());
            AddOwnedId(aNetworkSync.GetId());
        }

        // Id's requested from a network sync that this client hasn't instantiated, and the server hasn't created, need to the requested from the server
        // This handles the case where a NetworkSync is assigned to an object inside of a scene
        /*
        Debug.Log("ClientNetwork::RequestNetworkId - Requesting a network id for network sync " + aNetworkSync.name);

        // Let the server know about the new object
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.ConnectNetworkSync);
        sendMsg.Write(aNetworkSync.position.x);
        sendMsg.Write(aNetworkSync.position.y);
        sendMsg.Write(aNetworkSync.position.z);
        sendMsg.Write(aNetworkSync.rotation.x);
        sendMsg.Write(aNetworkSync.rotation.y);
        sendMsg.Write(aNetworkSync.rotation.z);
        sendMsg.Write(aNetworkSync.rotation.w);
        sendMsg.Write(aNetworkSync.gameObject.name);
        sendMsg.Write(aNetworkSync.sceneId);

        SendData(MessageReceiver.ServerOnly, ref stream, DeliveryMethod.ReliableOrdered);
        */
    }

    // Destroy a network object
    public void Destroy(int aObjectId)
    {
        if (!netIdObjMap.ContainsKey(aObjectId))
        {
            Debug.LogWarning("Unable to destroy unknown object " + aObjectId);
            return;
        }

        // Make a request to the server to delete the object
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.Destroy);
        sendMsg.Write(aObjectId);
        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.ReliableOrdered);
    }

    private void DestroyNetworkObjects(int aObjectId)
    {
        if (!netIdObjMap.ContainsKey(aObjectId))
        {
            Debug.LogWarning("Unable to destroy unknown object " + aObjectId);
            return;
        }

        Destroy(netIdObjMap[aObjectId].gameObject);
    }

    // Destroy all the network objects, except for ones that follow the client
    private void DestroyNetworkObjects(bool aDestroyFollowedByClient)
    {
        foreach (KeyValuePair<int, NetIdMapData> data in netIdObjMap)
        {
            if (data.Value.networkSync.followsClient && !aDestroyFollowedByClient)
            {
                continue;
            }
            Destroy(data.Value.gameObject);
        }
    }

    // Send voice data to the server
    public void SendVoiceData(int aNetObjId, int aCompressionType, int aDataLength, byte[] aData, int aVoiceDataId, VoiceChatChannel aChannel)
    {
        // Send the data to the server
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.VoiceData);
        sendMsg.Write(aNetObjId);
        sendMsg.Write(aCompressionType);
        sendMsg.Write(aVoiceDataId);
        sendMsg.Write((int)aChannel);
        sendMsg.Write(aDataLength);
        sendMsg.Write(aData);

        SendData(MessageReceiver.AllClients, ref stream, DeliveryMethod.UnreliableSequenced);
    }

    // Get the remote voice chat player for a given network object
    public AudioSource GetRemoteVoiceChatPlayer(int aNetworkId)
    {
        if (remoteVoiceChatPlayers.ContainsKey(aNetworkId))
        {
            VoiceChatPlayer player = remoteVoiceChatPlayers[aNetworkId].GetComponent<VoiceChatPlayer>();
            if (player)
            {
                return player.GetAudioSource();
            }
        }
        return null;
    }

    public void RemoveNetObject(int aNetworkId)
    {
        if (!netIdObjMap.ContainsKey(aNetworkId))
            Debug.LogError("RemoveNetObject called with a key that doesn't exist");
        else
            netIdObjMap.Remove(aNetworkId);
    }

    public bool AddNetObject(int aNetworkId, NetworkSync aNetworkSync, GameObject gameObject)
    {
        if (netIdObjMap.ContainsKey(aNetworkId))
        {
            Debug.LogError("AddNetObject called with a key that already exists: " + aNetworkId);
            DestroyObject(gameObject);
            return false;
        }

        NetIdMapData mapData = new NetIdMapData();
        mapData.gameObject = gameObject;
        mapData.networkSync = aNetworkSync;
        mapData.voiceChatPlayer = null;

        netIdObjMap[aNetworkId] = mapData;
        return true;
    }

    // Update is called once per frame
    virtual public void Update()
    {
        float initialTime = Time.realtimeSinceStartup;
        double netTime = GetTime();

        int statusMessages = 0;
        int dataMessages = 0;
        int connectionMessages = 0;
        int debugMessage = 0;
        int warningMessages = 0;
        int errorMessages = 0;
        int otherMessages = 0;

        // Read any new messages from the network
        uint packetSize;
        while (SteamNetworking.IsP2PPacketAvailable(out packetSize))
        {
            // Note: You'd think reusing these is better, but it's not really worth the hassle
            // https://stackoverflow.com/questions/4629666/want-to-re-use-memorystream/4629728#4629728
            MemoryStream stream = new MemoryStream();
            BinaryReader reader = new BinaryReader(stream);

            // TODO: We could reuse this instead of creating a new one each time
            byte[] messageData = new byte[packetSize];
            CSteamID sender;
            bool result = SteamNetworking.ReadP2PPacket(messageData, (uint)messageData.Length, out packetSize, out sender);
            sendingClientId = sender;

            //Debug.Log("Incoming data with size " + packetSize);

            dataMessages++;
            stream.Write(messageData, 0, messageData.Length);
            stream.Position = 0;
            HandleIncomingData(ref reader);
        }

        float deltaTime = (Time.realtimeSinceStartup - initialTime);
        int messagesProcessed = statusMessages + dataMessages + connectionMessages + debugMessage + warningMessages + errorMessages + otherMessages;
        if (messagesProcessed >= 100 || deltaTime > 1.0f)
        {
            Debug.Log(GetTime() + ": NetTime:" + netTime + " " + messagesProcessed + " network messages proccessed in " + (Time.realtimeSinceStartup - initialTime).ToString() + " seconds" +
                "\n\t statusMessages: " + statusMessages +
                "\n\t dataMessages: " + dataMessages +
                "\n\t connectionMessages: " + connectionMessages +
                "\n\t debugMessage: " + debugMessage +
                "\n\t warningMessages: " + warningMessages +
                "\n\t errorMessages: " + errorMessages +
                "\n\t otherMessages: " + otherMessages
                );
        }
    }

    // Write the parameters for an RPC call
    protected void WriteRPCParams(ref BinaryWriter aMsg, params object[] aParams)
    {
        // Build all the arguments up for the RPC
        string typeString = "";
        foreach (object arg in aParams)
        {
            if (arg is long)
            {
                typeString += "l";
            }
            else if (arg is ulong)
            {
                typeString += "u";
            }
            else if (arg is int)
            {
                typeString += "i";
            }
            else if (arg is float)
            {
                typeString += "f";
            }
            else if (arg is string)
            {
                typeString += "s";
            }
            else if (arg is bool)
            {
                typeString += "b";
            }
            else if (arg is Vector3)
            {
                typeString += "v";
            }
            else if (arg is Quaternion)
            {
                typeString += "q";
            }
            else
            {
                Debug.LogError("Unimplemented RPC parameter type " + arg.GetType());
                return;
            }
        }
        aMsg.Write(typeString);

        // Write all the parameters
        foreach (object arg in aParams)
        {
            if (arg is long)
            {
                aMsg.Write((long)arg);
            }
            else if (arg is ulong)
            {
                aMsg.Write((ulong)arg);
            }
            else if (arg is int)
            {
                aMsg.Write((int)arg);
            }
            else if (arg is float)
            {
                aMsg.Write((float)arg);
            }
            else if (arg is string)
            {
                aMsg.Write((string)arg);
            }
            else if (arg is bool)
            {
                aMsg.Write((bool)arg);
            }
            else if (arg is Vector3)
            {
                Vector3 vec = (Vector3)arg;
                aMsg.Write(vec.x);
                aMsg.Write(vec.y);
                aMsg.Write(vec.z);
            }
            else if (arg is Quaternion)
            {
                Quaternion quat = (Quaternion)arg;
                aMsg.Write(quat.x);
                aMsg.Write(quat.y);
                aMsg.Write(quat.z);
                aMsg.Write(quat.w);
            }
        }
    }

    protected void ReadRPCParams(ref List<object> aArgs, ref BinaryReader aMsg)
    {
        string argDef = aMsg.ReadString();
        foreach (char c in argDef)
        {
            if (c == 'l')
            {
                aArgs.Add(aMsg.ReadInt64());
            }
            else if (c == 'u')
            {
                aArgs.Add(aMsg.ReadUInt64());
            }
            else if (c == 'i')
            {
                aArgs.Add(aMsg.ReadInt32());
            }
            else if (c == 'f')
            {
                aArgs.Add(aMsg.ReadSingle());
            }
            else if (c == 's')
            {
                aArgs.Add(aMsg.ReadString());
            }
            else if (c == 'b')
            {
                aArgs.Add(aMsg.ReadBoolean());
            }
            else if (c == 'v')
            {
                float x = aMsg.ReadSingle();
                float y = aMsg.ReadSingle();
                float z = aMsg.ReadSingle();
                aArgs.Add(new Vector3(x, y, z));
            }
            else if (c == 'q')
            {
                float x = aMsg.ReadSingle();
                float y = aMsg.ReadSingle();
                float z = aMsg.ReadSingle();
                float w = aMsg.ReadSingle();
                aArgs.Add(new Quaternion(x, y, z, w));
            }
            else
            {
                Debug.LogError("Unhandled RPC parameter type " + c);
            }
        }
    }

    // Are we currently connected to another machine
    public bool IsConnected()
    {
        // TODO: Can we check if there are any active p2p connections? Should this check if you're in a lobby?
        return true; // connection.ConnectionsCount != 0;
    }

    // Transfer ownership of an object to another client
    public void TransferOwnership(int aNetId, CSteamID aClient)
    {
        RemoveOwnedId(aNetId);

        // Send a message to the other client that they have gained ownership of the object
        MemoryStream stream = new MemoryStream();
        BinaryWriter sendMsg = new BinaryWriter(stream);
        sendMsg.Write((int)MessageType.OwnershipGained);
        sendMsg.Write(aNetId);

        SendData(MessageReceiver.SingleClient, ref stream, DeliveryMethod.ReliableOrdered, 0, aClient);
    }

    // Add an ID to the list that I own
    protected void AddOwnedId(int aId)
    {
        // Tell the object that we are now the owner
        if (!netIdObjMap.ContainsKey(aId))
        {
            Debug.LogError("Client doesn't know about object " + aId + " which it is supposed to now own");
            return;
        }

        ownedIds.Add(aId);
        netIdObjMap[aId].gameObject.SendMessage("OnGainOwnership", SendMessageOptions.DontRequireReceiver);
    }
    // Remove an ID from the list of objects that I own
    protected void RemoveOwnedId(int aId)
    {
        // Tell the object that we just lost ownership
        if (!netIdObjMap.ContainsKey(aId))
        {
            Debug.LogError("Client doesn't know about object " + aId + " which it is supposed to lose ownership of");
            return;
        }

        ownedIds.Remove(aId);
        netIdObjMap[aId].gameObject.SendMessage("OnLoseOwnership");
    }
    public bool IsOwned(int aId)
    {
        return ownedIds.Contains(aId);
    }

    public string GetAddress()
    {
        // TODO: How can we get the IP address of this client
        return "";
    }

    public double GetTime()
    {
        // Lidgren.Network.NetTime.Now
        // TODO: How do we handle a time for network stuff?
        return Time.realtimeSinceStartup;
    }

    // Return the game object with the given network sync id
    public GameObject GetGameObject(int aNetworkId)
    {
        if (netIdObjMap.ContainsKey(aNetworkId))
        {
            return netIdObjMap[aNetworkId].gameObject;
        }
        return null;
    }
}
