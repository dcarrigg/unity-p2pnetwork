﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Steamworks;

// Example script for a title screen
public class TitleScreenLogic : MonoBehaviour
{
    // GameClient
    public GameClient client;

    // Friends list connection info
    [SerializeField]
    Dropdown friendList;
    List<P2PClientNetwork.FriendInfo> friends;

    void Start()
    {
        List<string> friendNames = new List<string>();

        // Populate the friends dropdown list
        friends = client.clientNet.GetFriends();
        for (int i = 0; i < friends.Count; ++i)
        {
            // Only add friends who are online
            string friendInfoString = friends[i].friendName + " - ";
            if (friends[i].inGame)
            {
                friendInfoString += "Playing " + (friends[i].gameInfo.m_gameID.m_GameID == 534100 ? "LaterSkaterTwo" : "other game");
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateOnline)
            {
                friendInfoString += "Online";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateAway)
            {
                friendInfoString += "Away";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateBusy)
            {
                friendInfoString += "Busy";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateLookingToPlay)
            {
                friendInfoString += "Looking to play";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateLookingToTrade)
            {
                friendInfoString += "Looking to trade";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateSnooze)
            {
                friendInfoString += "Snooze";
            }
            else if (friends[i].state == EPersonaState.k_EPersonaStateOffline)
            {
                friendInfoString += "Offline";
            }
            friendNames.Add(friendInfoString);
        }

        friendList.ClearOptions();
        friendList.AddOptions(friendNames);
    }

    public void JoinFriend()
    {
        client.clientNet.JoinFriend(friends[friendList.value].steamId);
    }

    public void CreateNewGame()
    {
        client.clientNet.CreateGame();
    }
}
